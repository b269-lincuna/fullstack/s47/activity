//https://www.javascripttutorial.net/javascript-dom/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);

function updateFullName() {
  spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}
